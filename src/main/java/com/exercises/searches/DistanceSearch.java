package com.exercises.searches;

import com.exercises.graph.Graph;
import com.exercises.graph.Node;

public class DistanceSearch extends Search {

	public DistanceSearch(Graph graph) {
	    super(graph);
	}

	public String getDistances(String[] nodes) {
		Integer distance = 0;

		for (int i = 0; i < nodes.length; i++) {
			Node node = super.graph.getNode(nodes[i]);

			if (i < nodes.length -1) {
				Integer neighborWeight = node.getNeighborWeight(nodes[i + 1]);

				if (neighborWeight == null)
					return NO_ROUTE;
				else
					distance += neighborWeight;
			}
		}

		return distance.toString();
	}
}
