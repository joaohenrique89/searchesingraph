package com.exercises.searches;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.exercises.graph.Graph;
import com.exercises.graph.Node;
import com.exercises.routes.RouteNode;
import com.exercises.routes.Routes;

public class ShortestRouteSearch extends Search {

	public ShortestRouteSearch(Graph graph) {
		super(graph);
	}

	public Integer getShortesRoute(String node1Name, String node2Name) {
		Node node1 = graph.getNode(node1Name);
		Node node2 = graph.getNode(node2Name);

		if (node1 == null || node2 == null)
			return -1;

		return getShortesRoute(node1, node2);
	}

	private Integer getShortesRoute(Node node1, Node node2) {
		
		Routes routes = new Routes(convertNodeToRouteNode(node1));
		
		boolean hasWhereAdvance = advanceEdges(routes, routes.getStartNode(), routes.getStartNode().getNeighbors());;
		
		while(hasWhereAdvance){
			Collection<RouteNode> currentEdge = new ArrayList<RouteNode>(routes.getEdges().values());
			
			for (RouteNode edgeNode : currentEdge) {
				hasWhereAdvance = advanceEdges(routes, edgeNode, edgeNode.getNeighbors());
			}
		}
		
		return routes.getShortestRoute(node2);
	}

	private boolean advanceEdges(Routes routes, RouteNode node, Map<String, Node> neighbors) {
		boolean hasWhereAdvance = false;
		
		for (Node neighbor : neighbors.values()) {
			boolean temp = routes.advanceEdge(node, convertNodeToRouteNode(neighbor), graph.getNode(node.getName()).getNeighborWeight(neighbor.getName()));
			
			if(temp)
				hasWhereAdvance = true;
		}
		
		return hasWhereAdvance;
	}
	
	private RouteNode convertNodeToRouteNode(Node node) {
		RouteNode routeNode = new RouteNode();
		routeNode.setName(node.getName());
		routeNode.setNeighbors(node.getNeighbors());
		return routeNode;
	}

	
}
