package com.exercises.searches;

import java.util.Map;

import com.exercises.graph.Graph;
import com.exercises.graph.Node;

public class NumberOfTripsSearch extends Search {

	public NumberOfTripsSearch(Graph graph) {
	    super(graph);
	}

	public Integer getNumbersOfTrips(String node1Name, String node2Name, Integer maxStops) {
		Node node1 = super.graph.getNode(node1Name);
		Node node2 = super.graph.getNode(node2Name);

		if (node1 == null || node2 == null)
			return 0;

		return getNumbersOfTrips(node1, node2, maxStops, 0, 0);
	}

	private Integer getNumbersOfTrips(Node node1, Node node2, Integer maxStops, Integer depth, Integer numberOfTrips) {
		if (depth >= maxStops)
			return numberOfTrips;

		Map<String, Node> nodeNeighbors = node1.getNeighbors();

		if (nodeNeighbors.containsKey(node2.getName())) {
			return ++numberOfTrips;
		}

		for (Node neighbor : nodeNeighbors.values()) {
			numberOfTrips = getNumbersOfTrips(neighbor, node2, maxStops, depth++, numberOfTrips);
		}
		
		return numberOfTrips;
	}
}
