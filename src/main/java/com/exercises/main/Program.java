package com.exercises.main;

import java.io.IOException;
import java.util.logging.Logger;

import com.exercises.fileparser.ReadFile;
import com.exercises.graph.Graph;
import com.exercises.searches.DistanceSearch;
import com.exercises.searches.NumberOfTripsSearch;
import com.exercises.searches.ShortestRouteSearch;

public class Program {
	static Logger log = Logger.getLogger(Program.class.getName());

	public static void main(String[] args) {

		while (true) {
			String[] input = ReadFile.getContent("F:\\input.txt").split(",");
			
			Graph graph = new Graph();

			loadGaph(graph, input);

			DistanceSearch distanceSearch = new DistanceSearch(graph);

			System.out.println("Distance is: " + distanceSearch.getDistances(new String[] { "A", "B", "C"})); 
			System.out.println("Distance is: " +  distanceSearch.getDistances(new String[] { "A", "D"})); 
			System.out.println("Distance is: " + distanceSearch.getDistances(new String[] { "A", "D", "C"})); 
			System.out.println("Distance is: " + distanceSearch.getDistances(new String[] { "A", "E", "B", "C", "D"})); 
			System.out.println("Distance is: " + distanceSearch.getDistances(new String[] { "A", "E", "D"})); 

			
			NumberOfTripsSearch numberOfTripsSearch = new NumberOfTripsSearch(graph);
			System.out.println("max number of trips is: " + numberOfTripsSearch.getNumbersOfTrips("C", "C", 3)); 
			System.out.println("max number of trips is: " + numberOfTripsSearch.getNumbersOfTrips("A", "C", 4)); 

			
			ShortestRouteSearch shortestRouteSearch = new ShortestRouteSearch(graph);
			System.out.println("shortest route from A to C is: " + shortestRouteSearch.getShortesRoute("A", "C")); 
			System.out.println("shortest route is from B to B: " + shortestRouteSearch.getShortesRoute("B", "B")); 

			
			
			try {
				System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void loadGaph(Graph graph, String[] input) {
		for (int i = 0; i < input.length; i++) {
			String temp = input[i].trim();
			
			graph.addRelation(temp.substring(0, 1),
					temp.substring(1, 2),
					Integer.valueOf(temp.substring(2, temp.length())));
		}
	}
}
