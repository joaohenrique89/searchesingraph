package com.exercises.graph;

import java.util.HashMap;
import java.util.Map;

public class Node {

	private String name;

	Map<String, Node> neighbors = new HashMap<String, Node>();

	Map<String, Integer> neighborsWeights = new HashMap<String, Integer>();

	public Node(String name) {
		this.name = name;
	}

	public void addNeighbor(Node node, Integer weight) {
		neighbors.put(node.getName(), node);
		neighborsWeights.put(node.getName(), weight);
	}

	public String getName() {
		return name;
	}

	public Node getNeighbor(String string) {
		return neighbors.get(string);
	}

	public Integer getNeighborWeight(String string) {
		return neighborsWeights.get(string);
	}

	public Map<String, Node> getNeighbors() {
		return neighbors;
	}

}
