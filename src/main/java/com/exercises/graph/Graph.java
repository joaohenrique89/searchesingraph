package com.exercises.graph;

import java.util.HashMap;
import java.util.Map;

public class Graph {

	private Map<String, Node> nodes = new HashMap<String, Node>();
	
	public int size(){
	    return nodes.size();
	}

	public void addRelation(String nameNode1, String nameNode2, Integer weight) {
		Node node1 = nodes.get(nameNode1);
		if (node1 == null) {
			node1 = new Node(nameNode1);
			nodes.put(nameNode1, node1);
		}

		Node node2 = nodes.get(nameNode2);
		if (node2 == null) {
			node2 = new Node(nameNode2);
			nodes.put(nameNode2, node2);
		}
		node1.addNeighbor(node2, weight);
	}

	public Node getNode(String string) {
		return nodes.get(string);
	}
}
