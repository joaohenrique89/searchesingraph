package com.exercises.routes;

import java.util.HashMap;
import java.util.Map;

import com.exercises.graph.Node;

public class Routes {

	private RouteNode startNode;

	private Map<String, RouteNode> edges = new HashMap<String, RouteNode>();

	public Routes(RouteNode startNode) {
		this.startNode = startNode;
	}

	public Map<String, RouteNode> getEdges() {
		return edges;
	}

	public void setEdges(Map<String, RouteNode> edges) {
		this.edges = edges;
	}

	public RouteNode getStartNode() {
		return startNode;
	}

	public boolean advanceEdge(RouteNode previous, RouteNode next, Integer weight) {
		RouteNode nextNode = edges.get(next.getName());

		if (nextNode == null || previous.getWeight() + weight < nextNode.getWeight()) {
			edges.put(next.getName(), next);
			next.setWeight(previous.getWeight() + weight);
			next.setPreviousNode(previous);
			
			return true;
		} 
		
		return false;
	}

	public int getShortestRoute(Node node) {
		 return edges.get(node.getName()).getWeight();
	}
}
