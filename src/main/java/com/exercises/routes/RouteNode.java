package com.exercises.routes;

import java.util.Map;

import com.exercises.graph.Node;

public class RouteNode {

    private int weight = 0;

    private String name;
    
    private RouteNode previousNode;
    
    private Map<String, Node> neighbors;
    
    
    public void setPreviousNode(RouteNode previousNode) {
        this.previousNode = previousNode;
    }
    public RouteNode getPreviousNode() {
        return previousNode;
    }
    
    
    public void setWeight(int weight) {
        this.weight = weight;
    }
    public int getWeight() {
        return weight;
    }

    
    
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    
    
	public Map<String, Node> getNeighbors() {
		return neighbors;
	}
	public void setNeighbors(Map<String, Node> neighbors) {
		this.neighbors = neighbors;
	}

}
