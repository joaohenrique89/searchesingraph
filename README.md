# README #

Just download it, import in Eclipse using Import -> Maven -> Existing Maven Project.

### What is this repository for? ###
 
Implementation of weighted graph with O(1) access to any node in it, and many searches algorithms including Dijkstra's for shortest path.
  

### Who do I talk to? ###

* joaohenrique89@gmail.com
